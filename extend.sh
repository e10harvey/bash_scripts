#Extend your monitor on most lab machines.  If this doesn't work run
#xrandr to view what the monitor is called.
xrandr --output VGA-1 --auto --output VGA-1 --auto --right-of DVI-I-1
xrandr --output HDMI-1 --auto --output HDMI-1 --auto --right-of DVI-I-1
